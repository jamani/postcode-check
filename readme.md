# Postcode-Check

## Installation

Add this to your composer.json:

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/dekbed-discounter/postcode-check.git"
    }
],
```

Install this package with composer:
```
composer require dekbed-discounter/postcode-check
```

Copy the config file:
```
php artisan vendor:publish --provider="DD\PostcodeCheck\ServiceProvider"
```

This will copy the postcode-check.php file to the config folder of your project.

Add the following key to your .ENV:
```
POSTCODE_API_PRO6PP_KEY=[YOUR_KEY]
```

## Examples

### Route
```
Route::get('postcode-check/nl/{postcode}/{houseNumber}/{extension?}',
    'PostcodeCheckController@getAddressNL')->name('postcode-check-nl');
```

### Controller
```
<?php

namespace App\Http\Controllers;

use DD\PostcodeCheck\Provider\Pro6PP_NL;
use Illuminate\Http\Request;
use PostcodeCheck;

class PostcodeCheckController extends Controller
{
    public function getAddressNL(Request $request)
    {
        return PostcodeCheck::getAddress(new Pro6PP_NL(), $request);
    }
}

```