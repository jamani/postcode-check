<?php

return [

    'Pro6PP_NL' => [
        'url' => 'https://api.pro6pp.nl/v1/autocomplete',
        'key' => env('POSTCODE_API_PRO6PP_KEY')
    ],
];