<?php

namespace DD\PostcodeCheck;

class PostcodeCheck
{
    /**
     * @param $provider
     * @param $request
     * @return mixed
     */

    public function getAddress($provider, $request)
    {
        $postcode = $request['postcode'];
        $houseNumber = $request['houseNumber'];
        $extension = $request['extension'];

        return $provider->findByPostcodeAndHouseNumber($postcode, $houseNumber, $extension);
    }
}