<?php

namespace DD\PostcodeCheck\Provider;

use DD\PostcodeCheck\Entity\Address;
use GuzzleHttp\Client;
use Log;

class Pro6PP_NL implements ProviderInterface
{
    /**
     * @param $postcode
     * @param $houseNumber
     * @param $extension
     * @return string
     */
    public function findByPostcodeAndHouseNumber($postcode, $houseNumber, $extension)
    {
        $clientParameters = [
            'http_errors' => true,
            'verify' => config('app.guzzle_ssl_verify'),
        ];

        $client = new Client($clientParameters);

        try
        {
            $response = $client->request('GET', config('postcode-check.Pro6PP_NL.url'), [
                'query' => [
                    'auth_key' => config('postcode-check.Pro6PP_NL.key'),
                    'nl_sixpp' => $postcode,
                    'streetnumber' => $houseNumber,
                    'extension' => $extension
                ]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
        }
        catch(\Exception $exception)
        {
            $context = [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'stack' => $exception->getTraceAsString()
            ];

            Log::log('error', 'Call to Pro6PP_NL postcode API failed', $context);
        }

        $address = new Address();

        if (isset($response['status']) && $response['status'] === 'ok')
        {
            $address->setPostcode($postcode);
            $address->setHouseNumber($houseNumber);
            $address->setStreet($response['results'][0]['street']);
            $address->setCity($response['results'][0]['city']);
            $address->setProvince($response['results'][0]['province']);
        }

        return json_encode($address->toArray(), JSON_PRETTY_PRINT);
    }
}