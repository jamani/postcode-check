<?php

namespace DD\PostcodeCheck\Provider;

interface ProviderInterface
{
    public function findByPostcodeAndHouseNumber($postcode, $houseNumber, $extension);
}